Adam Sherwood CV
===

To compile, XeLaTeX is needed. There are also some sty files that are covered by
the texlive-full package, but can also be found in the smaller texlive-extra and
texlive-font-extra packages.

You may need to install missing fonts based on errors in compilation.
